import { RatesProvider } from "../exchanges";

export default class CbrDaily implements RatesProvider {
  private static url = "https://www.cbr-xml-daily.ru/daily_json.js";
  private network: any;

  constructor(network: any) {
    this.network = network;
  }

  async getRate(from: string): Promise<number | null> {
    const resp = await this.network.get(CbrDaily.url);
    const rate = resp.data.Valute[from.toUpperCase()];

    return (rate && rate.Value) || null;
  }
}
