import { RatesProvider } from "../exchanges";

export default class EcbEurope implements RatesProvider {
  private static url =
    "http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml";
  private network: any;
  private parser: DOMParser;

  constructor(network: any) {
    this.network = network;
    this.parser = new DOMParser();
  }

  async getRate(from: string, to: string): Promise<number | null> {
    const { data: resp } = await this.network.get(EcbEurope.url);

    const xmlNode = this.parser.parseFromString(resp, "text/xml");
    const currNode = xmlNode.querySelector(`[currency="${to.toUpperCase()}"]`);

    return (currNode && currNode.attributes.rate.value) || null;
  }
}
