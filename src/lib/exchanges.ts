import axios from "axios";
import CbrDaily from "./providers/CbrDaily";
import EcbEurope from "./providers/EcbEurope";

export interface RatesProvider {
  getRate(from: string, to?: string): Promise<number | null>;
}

interface RatesAggregator {
  providers: RatesProvider[];
  currency: [string, string];
  setCurrency(from: string, to: string): void;
  getCurrentRate(): Promise<number | null>;
}

export default class ExchangeRate implements RatesAggregator {
  providers: RatesProvider[];
  currency: [string, string];

  constructor(from: string, to: string) {
    this.providers = [EcbEurope, CbrDaily].map(
      providerClass => new providerClass(axios)
    );
    this.currency = [from, to];
  }

  setCurrency(from: string, to: string) {
    this.currency = [from, to];
  }

  async getCurrentRate() {
    let rate = null;

    for (const provider of this.providers) {
      try {
        rate = await provider.getRate(...this.currency);
        break;
      } catch (e) {
        continue;
      }
    }

    return rate;
  }
}
