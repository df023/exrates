module.exports = {
  theme: {
    fontSize: {
      14: "1.4rem",
      16: "1.6rem",
      18: "1.8rem",
      26: "2.6rem"
    },
    spacing: {
      10: "1rem",
      26: "2.6rem"
    },
    extend: {}
  },
  variants: {},
  plugins: []
};
